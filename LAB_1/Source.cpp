#include "lexic.h"

int Attributes[256];
Tsymbol symbol;
int lexCode, pos;
char* buf = new char[256];
bool SuppressOutput;
FILE* FINP, * FOUT;

Tsymbol Gets()
{
	Tsymbol result;
	result.value = new char[2];
	fgets(result.value, 2, FINP);
	result.attr = Attributes[(int)result.value[0]];
	return result;
}

bool TabSearch(Tabl* tbl, char* bf, int& i, int size)
{
	i = 0;
	while (i < size && strcmp(tbl[i].key, ""))
	{
		if (!strcmp(tbl[i].key, bf))
		{
			return true;
		}
		i++;
	}
	return false;
}

int main()
{
	if (bool bl = scanner())
	{
		cout << endl << "Lexical analysis result is here!!!"<<endl;
	}
	else { cout << endl << "Something wrong happened..."<<endl; }
	FOUT = fopen("labres.txt", "r");
	char* str = new char[255];
	while (!feof(FOUT)) {
		fgets(str,255 , FOUT);
		printf("%s", str);
	}
	return 0;
}
