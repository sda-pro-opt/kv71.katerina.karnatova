#include "lexic.h"

bool scanner()
{
	bool OK = true;
	Tabl KeyTabl[9] = {

							{(char*)"PROCEDURE",401},
							{(char*)"VAR",402},
							{(char*)"BEGIN",403},
							{(char*)"END",404},
							{(char*)"COMPLEX",405},
							{(char*)"INTEGER",406},
							{(char*)"FLOAT",407},
							{(char*)"BLOCKFLOAT",408},
							{(char*)"EXT",409}
	};


	Tabl IdnTabl[100];

	for (int i = 0; i < 100; i++)
	{
		IdnTabl[i].key = (char*)"";
		IdnTabl[i].number = 1001 + i;
	}

	for (int i = 0; i < 256; i++)
	{
		if ((i == 9) || (i == 10) || (i == 32))					// whitespace
			Attributes[i] = 0;
		else if ((i == 40))										// ��������
			Attributes[i] = 3;
		else if ((i == 46) || (i == 44) || (i == 59) || (i == 58) || (i == 41))	// ���������
			Attributes[i] = 4;
		else if ((i > 64) && (i < 91))							// �������������
			Attributes[i] = 2;
		else if ((i > 47) && (i < 58))							//�����
			Attributes[i] = 1;
		else													//���������� ������
			Attributes[i] = 5;

	}


	cout << "Enter program name: ";
	cin >> buf;
	FINP = fopen(buf, "r");
	FOUT = fopen("labres.txt", "w+");
	if (feof(FINP))
	{
		OK = false;
		cout << "File is empty!";
	}
	else
	{
		fprintf(FOUT, "Encoded token string:\n");
		symbol = Gets();
		do
		{
			strcpy(buf, "");
			lexCode = 0;
			SuppressOutput = false;
			switch (symbol.attr)
			{
			case 0:              //whitespace
				while (!feof(FINP))
				{
					symbol = Gets();
					if (symbol.attr != 0)
					{
						break;
					}
				}
				SuppressOutput = true;
				break;
			case 1:
				OK = false;
				cout << "Error! Using numerical constants is forbidden!" << endl;
				symbol = Gets();
				fprintf(FOUT, "  Error! Using numerical constants is forbidden! ");
				SuppressOutput = true;
				break;
			case 2:              //ident
				while (!feof(FINP) && (symbol.attr == 2 || symbol.attr == 1))
				{
					strcat(buf, symbol.value);
					symbol = Gets();
				}

				if (TabSearch(KeyTabl, buf, pos, (sizeof(KeyTabl) / sizeof(Tabl))))
				{
					lexCode = KeyTabl[pos].number;

				}
				else
				{
					if (TabSearch(IdnTabl, buf, pos, (sizeof(IdnTabl) / sizeof(Tabl))))
					{
						lexCode = IdnTabl[pos].number;

					}
					else
					{
						lexCode = IdnTabl[pos].number;
						IdnTabl[pos].key = new char(strlen(buf) + 1);
						strcpy(IdnTabl[pos].key, buf);
					}
				}
				break;
			case 3:              //��������
				if (feof(FINP))
				{
					lexCode = (int)'(';
				}
				else
				{
					symbol = Gets();
					if (!strcmp(symbol.value, "*"))
					{
						if (feof(FINP))
						{
							OK = false;
							cout << "Error! Comment is not closed!";
							fprintf(FOUT, " Error! Comment is not closed! ");
							SuppressOutput = true;
						}
						else
						{
							symbol = Gets();
							do
							{
								while (!feof(FINP) && strcmp(symbol.value, "*"))
								{
									symbol = Gets();
								}
								if (feof(FINP))
								{
									OK = false;
									cout << "Error! Comment is not closed!";
									strcpy(symbol.value, "+");
									fprintf(FOUT, "  Error! Comment is not closed! ");
									SuppressOutput = true;
									break;
								}
								else
								{
									symbol = Gets();
								}
							} while (strcmp(symbol.value, ")"));
							if (!strcmp(symbol.value, ")"))
							{
								SuppressOutput = true;
							}
							if (!feof(FINP))
							{
								symbol = Gets();
							}
						}
					}
					else
					{
						lexCode = (int)'(';
					}
				}
				break;
			case 4:          //��������� ��� '('
				lexCode = (int)symbol.value[0];
				symbol = Gets();
				break;
			case  5:          //�������
				OK = false;
				cout << "Error! Symbol is not allowed!" << endl;
				symbol = Gets();
				fprintf(FOUT, " Error! Symbol is not allowed! ");
				SuppressOutput = true;
				break;
			}

			if (!SuppressOutput)
			{
				fprintf(FOUT, "%4d ", lexCode);
			}
		} while (!feof(FINP));

		getchar();
	}
	fprintf(FOUT, "\n\nIdentifier table:\n");
	int i = 0;
	while (i < (sizeof(IdnTabl) / sizeof(Tabl)) && strcmp(IdnTabl[i].key, ""))
	{
		fprintf(FOUT, "%11s %4d\n", IdnTabl[i].key, IdnTabl[i].number);
		i++;
	}
	fprintf(FOUT, "\n\nKeyword table:\n");
	i = 0;
	while (i < (sizeof(KeyTabl) / sizeof(Tabl)))
	{

		fprintf(FOUT, "%11s %4d\n", KeyTabl[i].key, KeyTabl[i].number);
		i++;
	}
	fprintf(FOUT, "\n\nSingle character delimiters:\n");
	fprintf(FOUT, "      )      41\n      ,      44\n      .      46\n      :      58\n      ;      59");
	fclose(FINP);
	fclose(FOUT);
	return OK;
}