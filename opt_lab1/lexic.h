#pragma once
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <malloc.h>

#pragma warning(disable: 4996)
using namespace std;

struct Tsymbol {
	char* value;
	int attr;
};

struct Tabl {
	char* key;
	int number;
};

extern int Attributes[256];
extern Tsymbol symbol;
extern int lexCode, pos;
extern char* buf;// = new char[256];
extern bool SuppressOutput;
extern FILE* FINP, * FOUT;

Tsymbol Gets();
bool TabSearch(Tabl* tbl, char* bf, int& i, int size);
bool scanner();

